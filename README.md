# Processo Seletivo 4linux

Repositório criado para versionar e armazenar os playbooks ansible, arquivos de configuração e arquivos d edocker compose desenvolvidos na etapa prática do processo seletivo da 4Linux.

Atalhos:

* [Prometheus roles](roles/prometheus/)
* [Node-exporter roles](roles/node-exporter/)
* [Arquivos de configuração](config_files/)
* [Docker compose](composes/)